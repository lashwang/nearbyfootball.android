package com.simon.nearbyfootball.misc;

import java.io.IOException;
import java.io.StringWriter;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;


public class JsonUtil {
	private static ObjectMapper m_object_mapper;

	private JsonUtil() {
		m_object_mapper = new ObjectMapper();
		m_object_mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public static ObjectMapper getObjectMapper() {
		if (m_object_mapper == null) {
			synchronized (JsonUtil.class) {
				m_object_mapper = new ObjectMapper();
				m_object_mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES,
						false);
			}
		}

		return m_object_mapper;
	}
	
}
