package com.simon.nearbyfootball.baidu.ui;

import android.app.Activity;
import android.os.Bundle;
import com.baidu.mapapi.map.MapView;
import com.simon.nearbyfootball.app.R;
import com.simon.nearbyfootball.baidu.adapter.BaiduMapViewAdapter;

public class BaiduMapView extends Activity {
	
	private BaiduMapViewAdapter mAdapter = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location);
		MapView view = (MapView) findViewById(R.id.bmapView);
		mAdapter = BaiduMapViewAdapter.getInstance(view);
		mAdapter.start();
		mAdapter.addOverlay(30.184597,120.141058);
	}

	@Override
	protected void onPause() {
		mAdapter.onPause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		mAdapter.onResume();
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		mAdapter.stop();
		super.onDestroy();
	}

}
