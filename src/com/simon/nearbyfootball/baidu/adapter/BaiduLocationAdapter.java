package com.simon.nearbyfootball.baidu.adapter;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.simon.nearbyfootball.app.AppContext;
import com.simon.nearbyfootball.app.R;
import com.simon.nearbyfootball.misc.Logger;

import android.content.Context;

public class BaiduLocationAdapter implements BDLocationListener{
	private static BaiduLocationAdapter INSTANCE = null;
	private BDLocationListener mLocationListerner = null;
	private LocationClient mLocClient;
	private Context mAppContext;
	private boolean mBackground = false;
	
	
	
	public static BaiduLocationAdapter getInstance(){
		if(INSTANCE == null){
			INSTANCE = new BaiduLocationAdapter();
			INSTANCE.init(AppContext.getAppContext());
		}
		return INSTANCE;
	}
	
	private void init(Context context){
		mAppContext = context.getApplicationContext();
		SDKInitializer.initialize(mAppContext);
		mLocClient = new LocationClient(mAppContext);
		
	}
	
	
	public void setLocationListener(BDLocationListener listerner){
		mLocationListerner = listerner;
	}

	@Override
	public void onReceiveLocation(BDLocation location) {
		// TODO Auto-generated method stub
		if(location == null){
			return;
		}
		Logger.debug(BDLocationDump(location));
		if(mLocationListerner != null){
			mLocationListerner.onReceiveLocation(location);
		}
	}
	
	public void start(){
		Logger.funtcionEntry();
		Logger.debug("mBackground:" + mBackground);
		if(mLocClient.isStarted()){
			mLocClient.stop();
		}
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);
		option.setCoorType("bd09ll");
		if(!mBackground){
			option.setScanSpan(1000);
		}else{
			option.setScanSpan(60000);
		}
		option.setIsNeedAddress(true);
		option.setProdName(mAppContext.getString(R.string.app_name));
		mLocClient.setLocOption(option);
		mLocClient.start();
		mLocClient.registerLocationListener(this);
		Logger.funtcionExit();
	}
	
	
	public void stop(){
		Logger.funtcionEntry();
		mLocClient.stop();
		mLocClient.unRegisterLocationListener(this);
		Logger.funtcionExit();
	}
	
	public boolean isStarted(){
		boolean isStarted = mLocClient.isStarted();
		
		Logger.debug("is loc client started:" + isStarted);
		return isStarted;
	}
	
	public void setIfBackbackgroundMode(boolean isBackgound){
		mBackground = isBackgound;
	}
	
	
	public String BDLocationDump(BDLocation location){
		return "addr: " + location.getAddrStr() +
			   ",city:" + location.getCity() +
			   ",cityCode:" + location.getCityCode() +
			   ",district:" + location.getDistrict() +
			   ",latitude:" + location.getLatitude() + 
			   ",longitude:" + location.getLongitude() +
			   ",radius:" + location.getRadius() +
			   ",locType:" + location.getLocType();
	}
	
	
	public static double getDistance(LatLng x,LatLng y){
		return DistanceUtil.getDistance(x, y);
	}
	
}
