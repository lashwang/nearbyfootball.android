package com.simon.nearbyfootball.app;

import java.io.IOException;
import java.io.InputStream;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

import com.simon.nearbyfootball.app.bean.City;
import com.simon.nearbyfootball.app.bean.CityList;
import com.simon.nearbyfootball.misc.JsonUtil;
import com.simon.nearbyfootball.misc.Logger;

import android.content.Context;

public class AppFactoryConfig {
	private static AppFactoryConfig config_ = null;
	private static final String ConfigFileName = "factoryConfig.json";
	private String host_;
	private NetBeanConfig queryPlace_;
	private City cityDefault_;
	
	private static final String urlHead = "http://";
	
	
	public static class NetBeanConfig{
		private String port_;
		private String requestParam_;
		public void setPort(String port){
			port_ = port;
		}
		
		public String getPort(){
			return port_;
		}
		
		
		public void setRequestParam(String param){
			requestParam_ = param;
		}
		
		
		public String getRequestParam(){
			return requestParam_;
		}
		
		
		public static NetBeanConfig loadFromJson(JsonParser jp) throws JsonParseException, IOException{
			NetBeanConfig config = new NetBeanConfig();
			
			while (jp.nextToken() != JsonToken.END_OBJECT) {
				String fieldname = jp.getCurrentName();
				jp.nextToken(); // move to value, or START_OBJECT/START_ARRAY
				if(fieldname.equals("port")){
					config.port_ = jp.getText();
				}else if(fieldname.equals("RequestParam")){
					config.requestParam_ = jp.getText();
				}
			}
			
			
			return config;
		}
	}
	
	
	public static void load(Context context){
		config_ = new AppFactoryConfig();
		InputStream is;
		
		try {
			is = context.getAssets().open(ConfigFileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		JsonFactory f = new JsonFactory();
		JsonParser jp;
		try {
			jp = f.createJsonParser(is);
			jp.nextToken();
			while (jp.nextToken() != JsonToken.END_OBJECT) {
				String fieldname = jp.getCurrentName();
				jp.nextToken(); // move to value, or START_OBJECT/START_ARRAY
				if(fieldname.equals("host")){
					config_.host_ = jp.getText();
				}else if(fieldname.equals("queryPlace")){
					//config_.queryPlace_ = JsonUtil.getObjectMapper().readValue(jp,NetBeanConfig.class);
					config_.queryPlace_ = NetBeanConfig.loadFromJson(jp);
				}else if(fieldname.equals("defaultCity")){
					config_.cityDefault_ = CityList.getInstance(AppContext.getAppContext()).findCityByName(jp.getText());
				}
				else{
					Logger.error("Unrecognized field:" + fieldname);
				}
			}
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				is.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		
		
		
	}
	

	public static City getDefaultCity(){
		return config_.cityDefault_;
	}
	
	public static String getHost(){
		return config_.host_;
	}
	
	public static AppFactoryConfig.NetBeanConfig getQueryPlaceConfig(){
		return config_.queryPlace_;
	}
	
	public static String getUrl(String host,NetBeanConfig config){
		StringBuilder sb = new StringBuilder();
		
		sb.append(urlHead)
		  .append(host)
		  .append(":")
		  .append(config.port_)
		  .append(config.requestParam_);
		
		return sb.toString();
	}
	
}
