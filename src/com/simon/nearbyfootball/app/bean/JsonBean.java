package com.simon.nearbyfootball.app.bean;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class JsonBean {
	public abstract void generateJsonObj(JSONObject json) throws JSONException;
}
