package com.simon.nearbyfootball.app.bean;

import org.json.JSONException;
import org.json.JSONObject;

public class SportsFrontQuery extends JsonBean{
	private String title;// 球场名 可空
	private Double longitude;// 经度 可空
	private Double latitude;// 维度 可空
	private String city;// 按城市查询 可空
	private Byte size;// '球场大小 5人制 7人制 等等', 可空
	private Byte light; // '光灯 1-有 0-无', 可空
	private Byte gazon;// '草皮类型 1-真草 2-人工草皮 3-塑胶 4-地板 5....' 可空
	private Integer pageNo;// 默认第一页 每页显示数量服务端控制 可空

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the size
	 */
	public Byte getSize() {
		return size;
	}

	/**
	 * @param size
	 *            the size to set
	 */
	public void setSize(Byte size) {
		this.size = size;
	}

	/**
	 * @return the light
	 */
	public Byte getLight() {
		return light;
	}

	/**
	 * @param light
	 *            the light to set
	 */
	public void setLight(Byte light) {
		this.light = light;
	}

	/**
	 * @return the gazon
	 */
	public Byte getGazon() {
		return gazon;
	}

	/**
	 * @param gazon
	 *            the gazon to set
	 */
	public void setGazon(Byte gazon) {
		this.gazon = gazon;
	}

	/**
	 * @return the pageNo
	 */
	public Integer getPageNo() {
		return pageNo;
	}

	/**
	 * @param pageNo
	 *            the pageNo to set
	 */
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	@Override
	public  void generateJsonObj(JSONObject json) throws JSONException {
		// TODO Auto-generated method stub
		JSONObject obj = json;
		
		if(title != null){
			obj.put("title", title);			
		}
		
		if(longitude != null){
			obj.put("longitude", longitude);			
		}
		
		if(latitude != null){
			obj.put("latitude", latitude);			
		}
		
		if(city != null){
			obj.put("city", city);			
		}
		
		if(size != null){
			obj.put("size", size);			
		}
		if(light != null){
			obj.put("light", light);			
		}
		if(gazon != null){
			obj.put("gazon", gazon);			
		}
		if(pageNo != null){
			obj.put("pageNo", pageNo);			
		}
	}

}
