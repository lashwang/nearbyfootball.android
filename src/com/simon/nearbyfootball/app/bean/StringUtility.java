package com.simon.nearbyfootball.app.bean;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtility {
	public static boolean hasChineseChar(String str){
		String regStr = "[\\u4e00-\\u9fa5]";
		
		Matcher matcher;
		
		matcher = Pattern.compile(regStr).matcher(str);
		
		if(matcher.find()){
			return true;
		}else{
			return false;
		}
	}
}
