package com.simon.nearbyfootball.app.bean;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.ObjectMapper;

import com.baidu.android.bbalbs.common.a.c;
import com.simon.nearbyfootball.misc.JsonUtil;
import com.simon.nearbyfootball.misc.Logger;

import android.content.Context;

public class CityList {
	private static List<City> m_city_list = new ArrayList<City>();
	private static List<City> m_province_list = new ArrayList<City>();
	private static CityList INSTANCE = null;
	
	
	public static final String[] hotCities = {
		"上海",
		"杭州",
		"北京",
		"广州",
		"深圳",
		"重庆",
		"天津",
		"成都"
	};
	
	private ObjectMapper objectMapper;
	
	
	public static CityList getInstance(Context context){
		if(INSTANCE == null){
			INSTANCE = new CityList();
			INSTANCE.init(context);
		}
		
		return INSTANCE;
	}
	
	private CityList(){
		objectMapper = JsonUtil.getObjectMapper();
	}
	
	public List<City> getCityList(){
		return m_city_list;
	}
	
	public List<City> getProvinceList(){
		return m_province_list;
	}
	
	
	private void init(Context context){
		InputStream is;
		try {
			is = context.getAssets().open("cityjson.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		JsonNode node;
		try {
			node = objectMapper.readTree(is);
			JsonNode cityList = node.get("result");
			if(cityList.isArray()){
				int size = cityList.size();
				for(int i = 0;i < size;i++){
					JsonNode cityNode = cityList.get(i);
					City city = parseCity(cityNode);
					m_city_list.add(city);
					if(city.getCityList().size() > 0){
						m_province_list.add(city);
					}
				}
			}			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try {
				is.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		
		
	}
	
	private City parseCity(JsonNode cityNode){
		City city = new City();
		JsonNode subNode = cityNode.get("cities"); 
		city.setEnglistName(cityNode.get("englishName").getTextValue());
		city.setId(cityNode.get("id").getTextValue());
		city.setName(cityNode.get("name").getTextValue());
		
		if(subNode != null && subNode.isArray()){
			int size = subNode.size();
			for(int i = 0;i < size;i++){
				city.addCityList(parseCity(subNode.get(i)));
			}
		}
		
		
		return city;
	}
	
	
	public City findCityByName(String cityName){
		
		Logger.debug("findCityByName,start to search city:" + cityName);
		
		for(City city:m_city_list){
			if(city.getName().equals(cityName)){
				Logger.finetrace("find city:" + city);
				return city;
			}
			
			if(city.getCityList().size() > 0){
				for(City subCity:city.getCityList()){
					if(subCity.getName().equals(cityName) || cityName.contains(subCity.getName())){
						Logger.finetrace("find city:" + subCity);
						return subCity;
					}
				}
			}
		}
		
		
		Logger.error("findCityByName,not in the city list:" + cityName);
		
		return null;
	}
	
	public List<City> search(String key){
		List<City> result = new ArrayList<City>();
		boolean hasChinese = StringUtility.hasChineseChar(key);
		
		
		for(City city:m_city_list){
			if(city.getCityList().size() > 0){
				for(City subCity:city.getCityList()){
					if(hasChinese){
						if(subCity.getName().contains(key)){
							result.add(subCity);
						}
					}else{
						if(subCity.getEnglishName().contains(key)){
							result.add(subCity);
						}
					}
				}
			}
			
		}
		
		return result;
	}
	
	
}
