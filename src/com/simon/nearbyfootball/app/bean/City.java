package com.simon.nearbyfootball.app.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

public class City{
	private String _id;
	private String _name;
	private String _english_name;
	private List<City> _city_list = new ArrayList<City>();
	
	public void setId(String id){
		_id = id;
	}
	
	
	public String getId(){
		return _id;
	}
	
	
	public void setName(String name){
		_name = name;
	}
	
	public String getName(){
		return _name;
	}
	
	
	public void setEnglistName(String english_name){
		_english_name = english_name;
	}
	
	public String getEnglishName(){
		return _english_name;
	}
	
	public void addCityList(City city){
		_city_list.add(city);
	}
	
	
	public List<City> getCityList(){
		return _city_list;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		
		sb.append(_id);
		sb.append("@");
		sb.append(_name);
		sb.append("@");
		sb.append(_english_name);
		
		return sb.toString();
	}
	
	
	
}
