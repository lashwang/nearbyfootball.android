package com.simon.nearbyfootball.app.bean;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.pm.PackageManager.NameNotFoundException;

import com.simon.nearbyfootball.app.AppContext;

public class DeviceInfo extends JsonBean{
	private static DeviceInfo INSTANCE = null;
	
	private String os = "";// 手机系统
	private String osVersion = "";// 手机系统版本
	private String channelNo = "";// 渠道号
	private Integer appVersion = null;// 客户端版本号
	private String imsi = "";
	private String imei = "";
	private String factory = "";// 厂商
	private String terminal = "";// 机型
	private String network = "";// 移动通信系统 2.5G 3G 4G等

	
	public static DeviceInfo getinstance(){
		if(INSTANCE == null){
			INSTANCE = new DeviceInfo();
		}
		
		return INSTANCE;
	}
	
	public void init(){
		os = "Android";
		osVersion = Integer.toString(android.os.Build.VERSION.SDK_INT);
		try {
			appVersion = AppContext.getAppVersionCode();
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			appVersion = null;
		}
		imei = AppContext.getImeiString();
		factory = android.os.Build.MANUFACTURER;
		terminal = android.os.Build.MODEL;
	}
	
	
	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getChannelNo() {
		return channelNo;
	}

	public void setChannelNo(String channelNo) {
		this.channelNo = channelNo;
	}

	public int getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(int appVersion) {
		this.appVersion = appVersion;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	@Override
	public void generateJsonObj(JSONObject json) throws JSONException {
		// TODO Auto-generated method stub
		if(os != null){
			json.put("os", os);
		}
		if(osVersion != null){
			json.put("osVersion", osVersion);
		}
		if(channelNo != null){
			json.put("channelNo", channelNo);
		}
		if(appVersion != null){
			json.put("appVersion", appVersion);
		}
		if(imsi != null){
			json.put("imsi", imsi);
		}
		if(imei != null){
			json.put("imei", imei);
		}
		if(factory != null){
			json.put("factory", factory);
		}
		if(terminal != null){
			json.put("terminal", terminal);
		}
		if(network != null){
			json.put("network", network);
		}
	}
}
