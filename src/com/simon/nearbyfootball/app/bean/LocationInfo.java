package com.simon.nearbyfootball.app.bean;

import com.baidu.location.BDLocation;
import com.baidu.mapapi.model.LatLng;
import com.simon.nearbyfootball.misc.Logger;

public class LocationInfo {
	private static LocationInfo INSTANCE = null;
	private LatLng mLocation;
	private String mCity;
	private boolean mLocated;


	public static LocationInfo getInstance(){
		if(INSTANCE == null){
			INSTANCE = new LocationInfo();
			INSTANCE.init();
		}
		return INSTANCE;
	}
	
	
	private void init(){
		mLocated = false;
	}
	
	
	public boolean isLocated(){
		return mLocated;
	}
	
	public LatLng getLocation(){
		return mLocation;
	}
	
	public void setLocation(BDLocation location){
		if(location == null){
			return;
		}
		
		mLocation = new LatLng(location.getLatitude(),location.getLongitude());
		mCity = location.getCity();
	}
	
	public void setLocated(boolean located){
		mLocated = located;
	}
	
	public String getCity(){
		return mCity;
	}
	
	
}
