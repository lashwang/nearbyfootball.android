package com.simon.nearbyfootball.app;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

public class AppContext {
	private static Context m_context = null;
	
	public static void setAppContext(Context context){
		m_context = context;
	}
	
	public static Context getAppContext(){
		return m_context.getApplicationContext();
	}
	
	
	public static String getImeiString(){
		TelephonyManager tm = (TelephonyManager) m_context.getSystemService(m_context.TELEPHONY_SERVICE); 
		
		return tm.getDeviceId().toLowerCase();
	}
	
	
    public static boolean isServiceRunning(Class<?> serviceClazz,Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClazz.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
		return false;
    }
    
    public static int getDpi(Activity activity){
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.densityDpi;
    }
    
    public static String getAppVersion() throws NameNotFoundException{
    	PackageInfo info = m_context.getPackageManager().getPackageInfo(m_context.getPackageName(), 0);  
    	
    	String versionName = info.versionName;  

        // 当前版本的版本号  
        int versionCode = info.versionCode;  
        
        return versionName;
    }
    
    public static int getAppVersionCode() throws NameNotFoundException{
    	PackageInfo info = m_context.getPackageManager().getPackageInfo(m_context.getPackageName(), 0);  
    	

        // 当前版本的版本号  
        int versionCode = info.versionCode;  
        
        return versionCode;
    }
    
    
}
