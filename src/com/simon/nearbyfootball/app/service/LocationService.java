package com.simon.nearbyfootball.app.service;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.mapapi.SDKInitializer;
import com.simon.nearbyfootball.app.AppConstant;
import com.simon.nearbyfootball.app.AppContext;
import com.simon.nearbyfootball.app.bean.LocationInfo;
import com.simon.nearbyfootball.baidu.adapter.BaiduLocationAdapter;
import com.simon.nearbyfootball.misc.Logger;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class LocationService extends Service implements BDLocationListener{
	private BaiduLocationAdapter mlocationAdapter;
	private SDKReceiver mReceiver;
	
	
	public class SDKReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String s = intent.getAction();
			
			Logger.debug("onReceive action:" + s);
			
			if (s.equals(SDKInitializer.SDK_BROADTCAST_ACTION_STRING_PERMISSION_CHECK_ERROR)) {
				// TODO:
			} else if (s.equals(SDKInitializer.SDK_BROADCAST_ACTION_STRING_NETWORK_ERROR)) {
				// TODO:
			}
		}
		
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Logger.dumpStackTrace();
		Logger.funtcionEntry();
		AppContext.setAppContext(this);
		registerBaiduSDKReceiver();
		mlocationAdapter = BaiduLocationAdapter.getInstance();
		mlocationAdapter.setLocationListener(this);
		mlocationAdapter.start();
		Logger.funtcionExit();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Logger.funtcionEntry();
		Logger.funtcionExit();
		
	};
	
	
	@Override 
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		Logger.funtcionEntry();
		Logger.funtcionExit();
	};
	

	@Override
	public void onReceiveLocation(BDLocation arg0) {
		// TODO Auto-generated method stub
		Logger.funtcionEntry();
		Logger.dumpStackTrace();
		if(arg0.getCity() == null){
			Logger.error("Get location failed");
			Logger.funtcionExit();
			return;
		}
		LocationInfo.getInstance().setLocation(arg0);
		if(!LocationInfo.getInstance().isLocated()){
			LocationInfo.getInstance().setLocation(arg0);
			Intent intent = new Intent(AppConstant.ACTION_LOCATION_UPDATE);
			sendBroadcast(intent);
			LocationInfo.getInstance().setLocated(true);
			Logger.finetrace("send location update broadcast");
		}
		BaiduLocationAdapter.getInstance().stop();
		Logger.funtcionExit();
	}
	
	private void registerBaiduSDKReceiver(){
		IntentFilter iFilter = new IntentFilter();
		iFilter.addAction(SDKInitializer.SDK_BROADTCAST_ACTION_STRING_PERMISSION_CHECK_ERROR);
		iFilter.addAction(SDKInitializer.SDK_BROADCAST_ACTION_STRING_NETWORK_ERROR);
		mReceiver = new SDKReceiver();
		registerReceiver(mReceiver, iFilter);
	}
	
	
}
