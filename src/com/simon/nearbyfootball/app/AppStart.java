package com.simon.nearbyfootball.app;


import com.simon.nearbyfootball.app.bean.CityList;
import com.simon.nearbyfootball.app.bean.DeviceInfo;
import com.simon.nearbyfootball.app.service.LocationService;
import com.simon.nearbyfootball.app.ui.CityChangeActivity;
import com.simon.nearbyfootball.app.ui.MainTabView;
import com.simon.nearbyfootball.misc.Logger;
import com.simon.nearbyfootball.misc.Logger.LogLvl;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public class AppStart extends Activity{

	
	@Override
    public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		Logger.setLogLevel(LogLvl.FINE_TRACE.lvl);
		Logger.funtcionEntry();
		appInit();
		startLocationService();
		setContentView(R.layout.start);
		final View view = View.inflate(this, R.layout.start, null);
		setContentView(view);
		showStartAnimation(view);
		int dpi = AppContext.getDpi(this);
		Logger.debug("Get system dpi:" + dpi);
		Logger.funtcionExit();
	}
	
	private void showStartAnimation(View view){
		AlphaAnimation aa = new AlphaAnimation(0.3f,1.0f);
		aa.setDuration(2000);
		view.startAnimation(aa);
		aa.setAnimationListener(new AnimationListener()
		{
			@Override
			public void onAnimationEnd(Animation arg0) {
				redirectTo();
			}
			@Override
			public void onAnimationRepeat(Animation animation) {}
			@Override
			public void onAnimationStart(Animation animation) {}
			
		});
	}
	
    private void redirectTo(){
    	Intent intent;
    	if(AppConfig.getInstance().getCity().isEmpty()){
    		intent = new Intent(this, CityChangeActivity.class);
    	}else{
    		intent = new Intent(this, MainTabView.class);
    	}
        startActivity(intent);
        finish();
    }
    
    private void startLocationService(){
    	Logger.debug("startLocationService");
        Intent intent=new Intent(this,LocationService.class);
        startService(intent);
    }
    
    
    private void appInit(){
    	AppContext.setAppContext(this);
    	DeviceInfo.getinstance().init();
		AppFactoryConfig.load(this);
		AppConfig.getInstance().load(this);
    }
}
