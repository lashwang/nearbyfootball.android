package com.simon.nearbyfootball.app;

import com.simon.nearbyfootball.misc.Logger;

import android.app.Application;

public class AppEntry extends Application{
	
	@Override
	public void onCreate() {
		super.onCreate();
		Logger.dumpStackTrace();
		Logger.funtcionEntry();
		Logger.funtcionExit();
	}
	
	
	
	
	@Override 
	public void onTerminate(){
		super.onTerminate();
		Logger.funtcionEntry();
		Logger.funtcionExit();
	}

}
