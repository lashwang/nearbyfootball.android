package com.simon.nearbyfootball.app.ui;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.simon.nearbyfootball.app.AppConfig;
import com.simon.nearbyfootball.app.R;
import com.simon.nearbyfootball.app.adapter.ApiClient;
import com.simon.nearbyfootball.app.bean.SportsFrontQuery;
import com.simon.nearbyfootball.misc.Logger;

import android.os.Bundle;
import android.util.Log;

public class SportsFrontListView extends BaseActivity
implements Listener<String>,ErrorListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_sport_front);
		
		
		SportsFrontQuery query = new SportsFrontQuery();
		query.setCity(AppConfig.getInstance().getCity());
		
		ApiClient.getSportFontList(this,query,this, this);
	}

	@Override
	public void onResponse(String response) {
		// TODO Auto-generated method stub
		Logger.finetrace("onResponse:" + response);
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		// TODO Auto-generated method stub
		Logger.error(error.getMessage());
	}
	
	
	
}
