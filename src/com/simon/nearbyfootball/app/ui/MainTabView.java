package com.simon.nearbyfootball.app.ui;

import com.simon.nearbyfootball.app.R;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

public class MainTabView extends TabActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_main);
		setTabs();
	}
	
	private void setTabs(){
		addTabs("场地",R.drawable.tab_main,SportsFrontListView.class);
		addTabs("约球",R.drawable.tab_activity,SportsFrontListView.class);
		addTabs("发布",R.drawable.tab_publish,SportsFrontListView.class);
		addTabs("我",R.drawable.tab_myself,SportsFrontListView.class);
		addTabs("菜单",R.drawable.tab_menu,SportsFrontListView.class);
	}
	
	private void addTabs(String labelId, int drawableId, Class<?> c){
		TabHost tabHost = getTabHost();
		Intent intent = new Intent(this, c);
		TabHost.TabSpec spec = tabHost.newTabSpec("tab" + labelId);	
		
		View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
		TextView title = (TextView) tabIndicator.findViewById(R.id.title);
		title.setText(labelId);
		ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
		icon.setImageResource(drawableId);
		
		spec.setIndicator(tabIndicator);
		spec.setContent(intent);
		tabHost.addTab(spec);
	}
	
}
