package com.simon.nearbyfootball.app.ui;

import com.simon.nearbyfootball.app.AppManager;
import com.simon.nearbyfootball.misc.Logger;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class BaseActivity extends Activity {

	private boolean allowFullScreen = true;

	private boolean allowDestroy = true;

	private View view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Logger.funtcionEntry();
		super.onCreate(savedInstanceState);
		allowFullScreen = true;
		AppManager.getAppManager().addActivity(this);
		Logger.funtcionExit();
	}

	@Override
	protected void onDestroy() {
		Logger.funtcionEntry();
		super.onDestroy();
		AppManager.getAppManager().finishActivity(this);
		Logger.funtcionExit();
	}

	public boolean isAllowFullScreen() {
		return allowFullScreen;
	}

	public void setAllowFullScreen(boolean allowFullScreen) {
		this.allowFullScreen = allowFullScreen;
	}

	public void setAllowDestroy(boolean allowDestroy) {
		this.allowDestroy = allowDestroy;
	}

	public void setAllowDestroy(boolean allowDestroy, View view) {
		this.allowDestroy = allowDestroy;
		this.view = view;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Logger.funtcionEntry();
		if (keyCode == KeyEvent.KEYCODE_BACK && view != null) {
			view.onKeyDown(keyCode, event);
			if (!allowDestroy) {
				Logger.funtcionExit();
				return false;
			}
		}
		boolean ret = super.onKeyDown(keyCode, event);
		Logger.funtcionExit();
		return ret;
	}
}
