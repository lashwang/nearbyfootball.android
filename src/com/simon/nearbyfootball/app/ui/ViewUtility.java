package com.simon.nearbyfootball.app.ui;

import android.view.View;

public class ViewUtility {
	public static void hideView(View view,boolean gone){
		if(gone){
			view.setVisibility(android.view.View.GONE);
		}else{
			view.setVisibility(android.view.View.INVISIBLE);
		}
	}
	
	
	public static void unhideView(View view){
		view.setVisibility(android.view.View.VISIBLE);
	}
}
