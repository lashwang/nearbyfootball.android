package androidparser.ui;

import java.io.File;
import java.io.IOException;

import logparser.AppConfig;
import logparser.ui.UIUtils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.xmlpull.v1.XmlPullParserException;

import androidparser.bean.ParseResource;

public class ResourceParserUI extends Shell {
	private Text text_project_path;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			ResourceParserUI shell = new ResourceParserUI(display);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the shell.
	 * 
	 * @param display
	 */
	public ResourceParserUI(Display display) {
		super(display, SWT.CLOSE | SWT.MIN | SWT.TITLE);
		setText("Android Resource Parser");

		Button btnSelectAndroidProject = new Button(this, SWT.NONE);
		btnSelectAndroidProject.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				File projectDir = UIUtils
						.showDirSelectDialog(ResourceParserUI.this);
				text_project_path.setText(projectDir.getPath());
			}
		});
		btnSelectAndroidProject.setBounds(461, 38, 129, 25);
		btnSelectAndroidProject.setText("Select Android Project");

		text_project_path = new Text(this, SWT.BORDER);
		text_project_path.setEditable(false);
		text_project_path.setBounds(10, 38, 445, 21);

		Button btnParse = new Button(this, SWT.NONE);
		btnParse.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				AppConfig.getInstance().setAndroidDir(
						text_project_path.getText());
				AppConfig.getInstance().saveConfig();
				try {
					ParseResource.toAnalysisFiles(text_project_path.getText());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					UIUtils.showErrorMessageBox(ResourceParserUI.this,
							e1.getMessage());
					return;
				} catch (XmlPullParserException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					UIUtils.showErrorMessageBox(ResourceParserUI.this,
							e1.getMessage());
					return;
				}
				
				UIUtils.showMessageBox(ResourceParserUI.this, "Done");
			}
		});
		btnParse.setBounds(10, 87, 580, 50);
		btnParse.setText("Parse");
		createContents();
		AppConfig.getInstance().loadConfig();
		text_project_path.setText(AppConfig.getInstance().getAndroidDir());
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setSize(606, 208);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
