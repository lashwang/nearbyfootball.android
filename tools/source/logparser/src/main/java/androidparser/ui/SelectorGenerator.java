package androidparser.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;

public class SelectorGenerator extends Shell {
	private Text text;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			SelectorGenerator shell = new SelectorGenerator(display);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the shell.
	 * @param display
	 */
	public SelectorGenerator(Display display) {
		super(display, SWT.CLOSE | SWT.MIN | SWT.TITLE);
		
		List list = new List(this, SWT.BORDER);
		list.setBounds(279, 50, 258, 212);
		
		Label lblAttr = new Label(this, SWT.NONE);
		lblAttr.setAlignment(SWT.RIGHT);
		lblAttr.setBounds(10, 57, 55, 15);
		lblAttr.setText("Attr Select");
		
		Combo combo = new Combo(this, SWT.READ_ONLY);
		combo.setBounds(72, 54, 110, 23);
		
		Button btnNewButton = new Button(this, SWT.NONE);
		btnNewButton.setBounds(194, 84, 55, 25);
		btnNewButton.setText(">>");
		
		Button button = new Button(this, SWT.NONE);
		button.setBounds(462, 10, 75, 25);
		button.setText("...");
		
		text = new Text(this, SWT.BORDER);
		text.setEditable(false);
		text.setBounds(10, 12, 446, 21);
		
		Button btnNewButton_1 = new Button(this, SWT.NONE);
		btnNewButton_1.setBounds(198, 207, 51, 25);
		btnNewButton_1.setText("<<");
		
		List list_1 = new List(this, SWT.BORDER);
		list_1.setBounds(10, 163, 172, 99);
		
		Label lblNewLabel = new Label(this, SWT.NONE);
		lblNewLabel.setBounds(10, 131, 55, 15);
		lblNewLabel.setText("Drawable\r\n");
		
		Button btnNewButton_2 = new Button(this, SWT.NONE);
		btnNewButton_2.setBounds(10, 282, 527, 25);
		btnNewButton_2.setText("Save as");
		createContents();
	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("AndroidSelectorGenerator");
		setSize(553, 358);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
