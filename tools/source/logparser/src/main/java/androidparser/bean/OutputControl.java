package androidparser.bean;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public abstract class OutputControl {
	protected ParseResource m_parser;
	protected String m_file_name;
	protected DataOutputStream m_output_stream;
	
	
	public OutputControl(ParseResource parser,String fileName) throws FileNotFoundException{
		m_parser = parser;
		m_file_name = fileName;
		m_output_stream = new DataOutputStream(new FileOutputStream(new File(m_parser.m_output_dir + m_file_name)));
	}
	public abstract void  writeToFile() throws IOException;
	
	
	protected void writeString(String str) throws IOException{
		m_output_stream.write(str.getBytes());
	}
	
	protected void closeStream() throws IOException{
		m_output_stream.close();
	}
	
	protected String getReletiveLayoutPath(String layout_path){
		String folder = new File(new File(layout_path).getParent()).getName();
		String name = new File(layout_path).getName();
		return new StringBuilder().append(folder).append("\\").append(name).toString();
	}
	
}
