package androidparser.bean;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import logparser.bean.Parser;

public class OutPutLayoutID extends OutputControl {

	private static final String head = "layout_path,view,id\n";
	
	
	public OutPutLayoutID(ParseResource parser, String file)
			throws FileNotFoundException {
		super(parser, file);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void writeToFile() throws IOException {
		// TODO Auto-generated method stub
		m_output_stream.write(Parser.utf8Head);
		m_output_stream.write(head.getBytes());
		for (String path : m_parser.m_layout_map.keySet()) {
			List<LayoutView> views = m_parser.m_layout_map.get(path);
			for (LayoutView view : views) {
				for (String property : view.m_view_property.keySet()) {
					if (property.equals("android:id")) {
						m_output_stream.write(toLines(path,view.m_view_name,
								view.m_view_property.get(property))
								.getBytes());
					}
				}
			}
		}
		m_output_stream.close();
	}

	private String toLines(String path,String view,String property) {
		return new StringBuilder().append(getReletiveLayoutPath(path))
				.append(",").append(view)
				.append(",").append(property)
				.append("\n").toString();
	}

}
