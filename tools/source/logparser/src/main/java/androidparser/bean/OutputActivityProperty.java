package androidparser.bean;

import java.io.FileNotFoundException;
import java.io.IOException;

import logparser.bean.Parser;

public class OutputActivityProperty extends OutputControl{

	public OutputActivityProperty(ParseResource parser, String fileName)
			throws FileNotFoundException {
		super(parser, fileName);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void writeToFile() throws IOException {
		// TODO Auto-generated method stub
		m_output_stream.write(Parser.utf8Head);
		writeHead();
		writeBody();
		closeStream();
	}
	
	private void writeHead() throws IOException{
		writeString(new String("name*,"));
		for(String item:m_parser.m_activity_property_set){
			if(item.contains("android:")){
				item = item.replace("android:", "") + "*";
			}
			writeString(new String(item + ","));
		}
		writeString("\n");	
	}
	
	private void writeBody() throws IOException{
		for(ActivityProperty activity:m_parser.m_activity_list){
			writeString(activity.m_property_map.get("android:name") + ",");
			for(String property:m_parser.m_activity_property_set){
				if(activity.m_property_map.containsKey(property)){
					writeString(activity.m_property_map.get(property));
					writeString(",");
				}else{
					writeString(",");
				}
			}
			writeString("\n");
		}
	}

}
