package androidparser.bean;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;

import logparser.bean.Parser;

public class OutputViewProperty extends OutputControl{
	private HashSet<String> m_properties_set = new HashSet<String>();
	private String m_view_name;
	
	public OutputViewProperty(ParseResource parser, String viewName)
			throws FileNotFoundException {
		super(parser, "View_" + viewName + ".csv");
		// TODO Auto-generated constructor stub
		
		for (String files : m_parser.m_layout_map.keySet()) {
			List<LayoutView> views = m_parser.m_layout_map.get(files);
			for (LayoutView view : views) {
				if(view.m_view_name.equals(viewName)){
					m_properties_set.addAll(view.m_view_property.keySet());
				}
			}
		}
		
		m_properties_set.remove("android:id");
		m_view_name = viewName;
	}

	@Override
	public void writeToFile() throws IOException {
		// TODO Auto-generated method stub
		m_output_stream.write(Parser.utf8Head);
		writeHead();
		writeBody();
		closeStream();
	}
	
	private void writeHead() throws IOException{
		writeString("layout_path,");
		
		for(String item:m_properties_set){
			if(item.contains("android:")){
				item = item.replace("android:", "") + "*";
			}
			writeString(new String(item + ","));
		}
		writeString("\n");		
	}
	
	private void writeBody() throws IOException{
		for (String layout_file : m_parser.m_layout_map.keySet()) {
			List<LayoutView> views = m_parser.m_layout_map.get(layout_file);
			for (LayoutView view : views) {
				if(view.m_view_name.equals(m_view_name)){
					StringBuilder sb = new StringBuilder();
					sb.append(getReletiveLayoutPath(layout_file)).append(",");
					for(String property:m_properties_set){
						if(view.m_view_property.containsKey(property)){
							sb.append(view.m_view_property.get(property)).append(",");
						}else{
							sb.append(",");
						}
					}
					sb.append("\n");
					writeString(sb.toString());
				}
			}
		}
	}
}
