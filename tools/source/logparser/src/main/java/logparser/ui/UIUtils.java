package logparser.ui;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class UIUtils {

	public static File showFileDialog(Shell shell,String[] filterExt){
		FileDialog dialog = new FileDialog(shell,SWT.OPEN);
		if(filterExt != null){
			dialog.setFilterExtensions(filterExt);
		}
		String path = dialog.open();
		if(path != null){
			return new File(path);
		}
		
		return null;
	}

	public static File showDirSelectDialog(Shell parent){
		DirectoryDialog dialog = new DirectoryDialog(parent);
		String dir = dialog.open();
		
		if(dir != null){
			return new File(dir);
		}
		
		return null;
	}

	public static void showMessageBox(Shell parent,String msg){
		MessageBox messageBox = new MessageBox(parent, SWT.ICON_WORKING | SWT.OK);
		messageBox.setMessage(msg);
		messageBox.open();
	}

	public static void showErrorMessageBox(Shell parent,String msg){
		MessageBox messageBox = new MessageBox(parent, SWT.ICON_ERROR | SWT.OK);
		messageBox.setMessage(msg);
		messageBox.open();
	}

}
