package logparser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class AppConfig {
	private static AppConfig INSTANCE = null;
	private String logcatPath = "";
	private String appTag = "";
	private String outputDir = "";
	private String androidDir = "";
	private static final String configFile = "config.properties";
	private static final String TAG_LOGCAT = "logcat";
	private static final String TAG_APP = "apptag";
	private static final String TAG_OUTPUTDIR = "outputDir";
	private static final String TAG_ANDROIDDIR = "androidDir";
	
	
	public static AppConfig getInstance(){
		if(INSTANCE == null){
			INSTANCE = new AppConfig();
		}
		
		return INSTANCE;
	}
	
	public void loadConfig(){
		Properties prop = new Properties();
		
		InputStream input = null;
		
		try {
			 
			input = new FileInputStream(configFile);
	 
			// load a properties file
			prop.load(input);
	 
			// get the property value and print it out
			logcatPath = prop.getProperty(TAG_LOGCAT);
			appTag = prop.getProperty(TAG_APP);
			outputDir = prop.getProperty(TAG_OUTPUTDIR);
			androidDir = prop.getProperty(TAG_ANDROIDDIR);
		} catch (IOException ex) {
			ex.printStackTrace();
			logcatPath = "";
			appTag = "";
			outputDir = "";
			androidDir = "";
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			if(appTag == null){
				appTag = "";
			}
			
			if(logcatPath == null){
				logcatPath = "";
			}
			
			if(outputDir == null){
				outputDir = "";
			}
			
			
			if(androidDir == null){
				androidDir = "";
			}
		}		
		
	}
	
	
	public void saveConfig(){
		Properties prop = new Properties();
		OutputStream output = null;
		
		
		try {
			 
			output = new FileOutputStream(configFile);
			// set the properties value
			if(logcatPath != null) prop.setProperty(TAG_LOGCAT, logcatPath);
			if(appTag != null) prop.setProperty(TAG_APP, appTag);
			if(outputDir != null) prop.setProperty(TAG_OUTPUTDIR, outputDir);
			if(androidDir != null) prop.setProperty(TAG_ANDROIDDIR, androidDir);
			// save properties to project root folder
			prop.store(output, null);
	 
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	 
		}
	}
	
	public void setLogcatPath(String path){
		logcatPath = path;
	}
	
	public String getLogcatPath(){
		return logcatPath;
	}
	
	public void setAppTag(String tag){
		appTag = tag;
	}
	
	public String getAppTag(){
		return appTag;
	}
	
	public void setOutputDir(String dir){
		outputDir = dir;
	}
	
	public String getOutputDir(){
		return outputDir;
	}
	
	public void setAndroidDir(String dir){
		androidDir = dir;
	}
	
	public String getAndroidDir(){
		return androidDir;
	}
}
