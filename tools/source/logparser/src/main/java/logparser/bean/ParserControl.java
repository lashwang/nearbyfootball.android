package logparser.bean;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ParserControl {
	private static ParserControl INSTANCE = null;
	private List<Parser> m_paser_list = null; 
	private IQueryProcess m_process_handler = null;
	
	
	public static ParserControl getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ParserControl();
		}
		
		return INSTANCE;
	}
	
	public void SetQueryProcessHandler(IQueryProcess handler){
		m_process_handler = handler;
	}
	
	private ParserControl(){
		m_paser_list = new ArrayList<Parser>();
		Parser parse;
		parse = new ParserOpenChannel();
		m_paser_list.add(parse);
		parse = new ParserNearbyFootball();
		m_paser_list.add(parse);
		parse = new ParserSystem();
		m_paser_list.add(parse);
	}
	
	public void setOutputFolder(String folder){
		new File(folder).mkdirs();
		for (Parser parser : m_paser_list) {
			parser.setOutputFolder(folder);
		}
	}
	
	
	public void processStart() throws Exception{
		for (Parser parser : m_paser_list) {
			parser.processStart();
		}
	}
	
	
	public void processLine(String line,int origline){
		ParserLine oneLine = new ParserLine();
		oneLine.SetQueryProcessHandler(m_process_handler);
		oneLine.processLine(line,origline);
		boolean processed;
		
		
		if(!oneLine.getValid()){
			return;
		}
		for (Parser parser : m_paser_list) {
			processed = parser.processLine(line, oneLine);
			if(processed){
				break;
			}
		}
	}
	
	
	public void processEnd(){
		for (Parser parser : m_paser_list) {
			parser.processEnd();
		}
	}
}
