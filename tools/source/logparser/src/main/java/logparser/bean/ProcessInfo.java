package logparser.bean;

public class ProcessInfo {
	private int m_pid;
	private String m_processName;
	
	
	public ProcessInfo(int pid,String processName){
		m_pid = pid;
		m_processName = processName;
	}
	
	
	public int getPid(){
		return m_pid;
	}
	
	public String getProcessName(){
		return m_processName;
	}
}
