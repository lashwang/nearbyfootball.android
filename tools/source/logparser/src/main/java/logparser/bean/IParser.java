package logparser.bean;

public interface IParser {
	public String toCSVHead();
	public String toCSVFormat(ParserLine parse);
}
