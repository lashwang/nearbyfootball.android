package logparser.bean;

public abstract class Parser {
	protected String m_output_folder;
	
	public static final byte[] utf8Head = {(byte) 0xEF,(byte) 0xBB,(byte) 0xBF};
	
	public void setOutputFolder(String folder){
		m_output_folder = folder;
	}
	
	
	
	public static String csvHandlerStr(String str) {
		
		if(str.contains(",")){
			if(str.contains("\"")){
				str = str.replaceAll("\"","\'");
			}
			
			return "\"" + str + "\"";			
		}
		
		
		return str;
    }
	
	public static String csvIntToStr(String str){
		if(str.isEmpty()){
			return str;
		}
		return "\'" + str + "\'"; 
	}
	
	
	abstract public void processStart() throws Exception;
	abstract public void processEnd();
	abstract public boolean processLine(String line,ParserLine parsed);
}
